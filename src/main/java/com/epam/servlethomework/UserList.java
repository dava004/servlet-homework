package com.epam.servlethomework;

import java.util.ArrayList;
import java.util.List;

public class UserList {
	private static UserList instance = new UserList();

	private final List<User> users;

	private UserList() {
		users = new ArrayList<User>();
		users.add(new User("admin", "admin"));
	}

	public static UserList getUserList() {
		return instance;
	}

	public boolean isExisting(User user) {
		return users.contains(user);
	}

	public boolean isUserNameReserved(String userName) {
		for (User user : users) {
			if (user.getUserName().equals(userName)) {
				return true;
			}
		}
		return false;
	}

	public boolean isValidPassword(String password) {
		return password != null && password.length() > 3;
	}
	
	public boolean isValidUserName(String userName) {
		return userName != null && userName.length() > 3;
	}

	public void add(User user) {
		users.add(user);
	}
}
