<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="reset.css">
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="UTF-8">
<title>Login Page</title>
</head>
<body>
	<h1>Please log in</h1>
	<form id="user_data" method="post" action="Login">
		<label for="user_name">User name</label> <input id="user_name"
			name="user_name" type="text"><br> <label for="password">Password</label><input
			id="password" name="password" type="password"><br>
		<button type="submit" value="Login">Login</button>
	</form>
	<a href="Submit">I don't have an account yet.</a>
</body>
</html>