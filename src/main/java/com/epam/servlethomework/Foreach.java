package com.epam.servlethomework;

import java.util.Collection;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Foreach extends SimpleTagSupport {

	private String var;
	private Collection items;

	public void setItems(Collection items) {
		this.items = items;
	}

	public void setVar(String var) {
		this.var = var;
	}

	@Override
	public void doTag() throws JspException, java.io.IOException {
		super.doTag();

		PageContext pageContext = (PageContext) getJspContext();
		
		for(Object o : items) {
			pageContext.setAttribute(var, o);
			getJspBody().invoke(null);
		} 
	}
}
