<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="reset.css">
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="UTF-8">
<title>Submit Page</title>
</head>
<body>
	<h1>Submit page</h1>
	<form id="user_data" method="post" action="Submit">
		<label for="user_name">User name</label> <input id="user_name"
			name="user_name" type="text"><br> <label for="password">Password</label><input
			id="password" name="password" type="password"><br>
		<button type="submit" value="Submit">Submit</button>
	</form>
	<a href="Login">I already have an account.</a>
</body>
</html>