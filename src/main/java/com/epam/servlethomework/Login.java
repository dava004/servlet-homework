package com.epam.servlethomework;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login extends HttpServlet {

	private static final long serialVersionUID = -7095937973614978175L;

	private static final String COOKIE_NAME = "OUT_REMEMBERME";
	private final Map<String, String> rememberMe = new HashMap<String, String>();
	private final UserList users = UserList.getUserList();

	@Override
	public void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException {
		for (Cookie c : request.getCookies()) {
			if (c.getName().equals(COOKIE_NAME)) {
				final String userName = rememberMe.get(c.getValue());
				if (userName != null) {
					final HttpSession session = request.getSession();
					synchronized (session) {
						session.setAttribute("userName", userName);
					}
					response.sendRedirect("Forum");
					return;
				}
			}
		}
		request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
	}

	@Override
	public void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException {
		final String userName = request.getParameter("user_name");
		final String password = request.getParameter("password");
		final User user = new User(userName, password);
		if (!users.isExisting(user)) {
			request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
			return;
		}
		
		final HttpSession session = request.getSession();
		synchronized (session) {
			session.setAttribute("userName", userName);
		}
		final String id = String.valueOf(System.nanoTime());
		final Cookie rememberMeCookie = new Cookie(COOKIE_NAME, id);
		rememberMeCookie.setMaxAge(60 * 30);
		response.addCookie(rememberMeCookie);
		rememberMe.put(id, userName);
		response.sendRedirect("Forum");
	}
}
