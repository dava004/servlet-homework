package com.epam.servlethomework;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Forum extends HttpServlet {

	private static final long serialVersionUID = -7560628946079165433L;
	private static List<Comment> comments = new ArrayList<Comment>();
	static {
		comments.add(new Comment("bela", new Date(), "en vagyok az elso"));
		comments.add(new Comment("geza", new Date(), "en vagyok a masodik"));
		comments.add(new Comment("pisti", new Date(), "en vagyok a harmadik"));
	}

	public static List<Comment> getComments() {
		return comments;
	}

	public void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException {
		final HttpSession session = request.getSession();
		String userName;
		synchronized (session) {
			userName = (String) session.getAttribute("userName");
		}
		if (userName == null) {
			response.sendRedirect("Login");
		} else {
			request.setAttribute("comments", comments);
			request.setAttribute("userName", userName);
			request.getRequestDispatcher("/WEB-INF/forum.jsp").include(request,
					response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String commentText = request.getParameter("comment");
		HttpSession session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		if (commentText != null && !commentText.equals("")) {
			comments.add(new Comment(userName, new Date(), commentText));
			request.setAttribute("comments", comments);
			request.setAttribute("userName", userName);
		}
		response.sendRedirect("Forum");
	}
}
