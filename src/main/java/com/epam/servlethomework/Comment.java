package com.epam.servlethomework;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment {
	private final String userName;
	private final Date dateOfComment;
	private final String commentText;
	
	public Comment(String userName, Date dateOfComment, String commentText) {
		super();
		this.userName = userName;
		this.dateOfComment = dateOfComment;
		this.commentText = commentText;
	}

	public String getUserName() {
		return userName;
	}

	public String getDateOfComment() {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		return format.format(dateOfComment);
	}

	public String getCommentText() {
		return commentText;
	}
}
