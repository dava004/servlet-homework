<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="MyTaglib" uri="MyTaglib"%>

<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="reset.css">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Forum</title>
</head>
<body>
	<h1>Forum</h1>
	
	<span class="user_name">Signed in as ${requestScope.userName}</span>
	<form method="post" action="Forum">
		<textarea name="comment" cols="30" rows="5"></textarea>
		<button type="submit" value="Send">Send</button>
	</form>

	<table>
		<MyTaglib:foreach var="comment" items="${requestScope.comments}">
			<tr>
				<td class="table_user">${comment.userName} on ${comment.dateOfComment}</td>
				<td>${comment.commentText}</td>
			</tr>
		</MyTaglib:foreach>
	</table>

</body>
</html>
